import React from 'react';
import "./Dashboard.css"
import dashimg from "./assets/images/dashboard.PNG"
const Dashboard = () => {
    return (
        <div className='dashBoard'>
            <img src={dashimg}/>
        </div>
    );
}

export default Dashboard;
