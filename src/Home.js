import React from 'react';
import Navigation from "./Navbar";
import Dashboard from "./Dashboard"
import Project from "./Project"

import {
    BrowserRouter as Router,
    Routes,
    Route,
    Link,
    Fragment
} from "react-router-dom";
const Home = () => {
    function changeMovement(pix) {
        document.getElementById("movement").style.marginLeft = pix;
    }
    return (
        <Router>
            <Navigation updateMovement={(e)=>{changeMovement(e)}}/>
            <div id='movement'>
            <Routes>
                <Route exact path="/" element={ <Dashboard />}/>
                <Route exact path="/project" element={<Project />}/>
                   
            </Routes>
            </div>
        </Router>
    );
}

export default Home;
