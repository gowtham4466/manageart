import React,{useState,useEffect} from 'react';
import {Tabs,Tab, Container,Col,Row} from 'react-bootstrap'
import "./Project.css"
import Textbox from './Textbox'
import ProjectCard from './ProjectCard'
import InfiniteScroll from "react-infinite-scroll-component";
import {getmeta} from './projectmeta'
import Skeleton, { SkeletonTheme } from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css'

const Project = () => {
    const [projdata,setProjData] = useState()
    const [count,setCount] = useState(3)
    const [hasMore, sethasMore] = useState(true)
    const [selectedObj, setSelectedObj] = useState('')
    const [skeleLoading, setSkeleLoading] = useState(true)
    let formatProject = [{key:'reqNum','label':'Request Number'},
                        {key:'initiatedBy','label':'Initiated By'},
                        {key:'initiatedDate','label':'Initiated Date'},
                        {key:'product','label':'Product'},
                        {key:'componentType','label':'Component Type'}]
    useEffect(()=>{
        let data = getmeta()
        console.log('useeffectcalling')
        let result = []
        for(let i=0;i<3;i++){
            result.push(data[i])
        }
        setProjData(result)
    },[])
    // let value = [
    //     {
    //         "imageSrc": img,
    //         "reqNum": '81p-1c',
    //         "initiatedBy": 'Designer',
    //         "initiatedDate": '31-Jul-2020',
    //         "product": 999,
    //         "componentType": 'Front Label',
    //         "status": 'Artwork Approved and Released'
    //     }
    // ]
    async function fetchMoreData() {
        let data = getmeta()        
        let result = []
        if(count<= data.length-3){
            for(let i=count;i<count+3;i++){
                result.push(data[i])
            }
            await setTimeout(() => {
                let temp = projdata.concat(result)
                console.log('temp',temp)
                setProjData(temp)
                setCount(count+3)
            }, 2000);
        } else{
            sethasMore(false)
        }
    }
    function selectedProject(id){
        document.getElementsByClassName('infinite-scroll-component__outerdiv')[0].style.width = '50%'
        document.getElementsByClassName('infinite-scroll-component__outerdiv')[0].style.float = 'left'        
        console.log(id)
        let data = getmeta()
        let result = data.filter(f=>{
            return f.id === id
        })[0]
        setSkeleLoading(true)
        setSelectedObj(result)
        setTimeout(() => {
            setSkeleLoading(false)
        }, 3000);
    }
    return (
        <div>
            <div className='toolkit'>
                <span className='toolchild'><i class="fas fa-th-large"></i></span>
                <span className='toolchild'><i class="fas fa-bars"></i></span>
                <input className='toolchild' type='text' placeholder='Filter items'/>
            </div>
            <div>
            {
                projdata ? (

            <InfiniteScroll
          dataLength={ projdata.length-1}
          next={()=>{fetchMoreData()}}
          hasMore={hasMore}
          className='cardArea'
          id="cardArea"
          loader={<h4>Loading...</h4>}
        >
            {
                projdata.length && projdata.map(v=>{
                    return(
                        <ProjectCard val={v} selectedObj={selectedObj} selectedProject={(id)=>{selectedProject(id)}}/>
                    )
                })
            }
            </InfiniteScroll>
                ) : ('')
            }
            {
                selectedObj ? (
                    <div className="cardDesc">
                        {
                            skeleLoading ? (
                                <Skeleton />                                
                            ) : (
                                <h1>
                                    <i class="fas fa-list" onClick={()=>{
                                        setSelectedObj('');
                                        document
                                        .getElementsByClassName('infinite-scroll-component__outerdiv')[0].style.width = '100%'
                                        setSkeleLoading(true)
                                    }}></i>
                                    {selectedObj.name}</h1>                                
                            )
                        }
                        <Tabs defaultActiveKey="info" id="uncontrolled-tab-example" className="mb-3">
                        <Tab eventKey="info" title="Info">
                        <Container>
                        <Row>
                            {formatProject.map(f=>{
                                return (
                                    <Col lg={4}>
                                        {
                                            skeleLoading ? (
                                                <Skeleton />
                                            ) : (
                                                <Textbox placeholder={f.label} value={selectedObj[f.key]} 
                                                change={(e)=>{
                                                    let temp = selectedObj
                                                    temp[e.key] = e
                                                    setSelectedObj({...temp})
                                                }}/>
                                            )
                                        }
                                    </Col>
                                )
                            })}
                        </Row>
                        </Container>   
                        </Tab>
                        <Tab eventKey="files" title="Files">
                          Files
                        </Tab>
                        <Tab eventKey="stake" title="Stakeholders">
                          stackholder
                        </Tab>
                        <Tab eventKey="activity" title="Activity log">
                          activitylog
                        </Tab>
                      </Tabs>
                    </div>
                ) : ('')
            }
            </div>
        </div>
    );
}

export default Project;
