import React from 'react';
import "./Textbox.css"
const Textbox = (props) => {
    return (
        <div className="field-wrapper">
            <input type='text' onChange={(e)=>{
                props.change(e.target.value)}} value={props.value}/>
                <div className="field-placeholder"><span>{props.placeholder}</span></div>
        </div>
    );
}

export default Textbox;
