import React, {useState,useEffect} from 'react';
import "./Navbar.css"
import logo from "./assets/images/Capture.PNG"
import {
    BrowserRouter as Router,
    Link,
} from "react-router-dom";
const Navigation = (props) => {
    const [showicon,setShowicon] =  useState(true)
    const [navHeader, setNavHeader] = useState('Dashboard')
    useEffect(()=>{

    },[])
    function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
        document.getElementById("main").style.marginLeft = "250px";
        props.updateMovement('250px')
        setShowicon(false)
        Object.values(document.getElementsByClassName('contentPart')).forEach(e => {
            e.style.marginTop = "1%";
            e.style.marginRight = "2%"
            e.style.marginBottom = "1%"
            e.style.marginleft = "3%"
        });
      }
      
      function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("main").style.marginLeft= "0";
        props.updateMovement('0px')  
        Object.values(document.getElementsByClassName('contentPart')).forEach(e => {
            e.style.marginTop = "2%";
            e.style.marginRight = "4%"
            e.style.marginBottom = "2%"
            e.style.marginleft = "4%"
        });      
        setShowicon(true)        
      }
    return (
        <div>
            <div id="mySidenav" className="sidenav">
            <img src={logo}/>
                <span href="#" className="closebtn" onClick={()=>{closeNav()}}>&lt;&#9776;</span>
                <Link to="/" onClick={()=>{setNavHeader('Dashboard')}}>Dashboard</Link>
                <Link to="/project" onClick={()=>{setNavHeader('New Project')}}>Start a new Project</Link>
                <hr/>
                <span>workflows</span>
                <Link to="#">Inbox(3)</Link>
                <Link to="#">MyProject(4)</Link>
                <Link to="#">AllProject(4)</Link>                
                <hr/>
                <span>Assets library</span>
                <Link to="#">search)</Link>
                <Link to="#">Browse</Link>
                <hr/>
            </div>

            <div id="main">
                {
                    showicon ? (
                        <span style={{'fontSize':'30px','cursor':'pointer'}} onClick={()=>{openNav()}}>&#9776;</span>
                    ) : ('')
                }
            </div>
            <div className="navheader">
                <h3>{navHeader}</h3>
            </div>
            <div className='navicons'>
                <span className='icons'>?</span>
                <span className='icons' id="nameicon">GR</span>    
            </div>
        </div>
    );
}

export default Navigation;
