import React from 'react';
import "./Project.css"

const ProjectCard = (props) => {
    return (
        <div className={`cards ${props.selectedObj.id === props.val.id ? ' selectedcard': ''}`} onClick={()=>{props.selectedProject(props.val.id)}}>
                            <div className='imagePart'><img src={props.val.imageSrc}/></div>
                            <div className='contentPart'>
                                <div className='contentHeader'>Request Number</div>
                                <div>{props.val.reqNum}</div>
                                
                                
                            </div>
                            <div className='contentPart'>
                                <div className='contentHeader'>Initiated By</div>
                                <div>{props.val.initiatedBy}</div>
                            </div>
                            <div className={`contentPart ${props.selectedObj ? ' hidecontent': ''}`}>
                                <div className='contentHeader'>Initiated Date</div>
                                <div>{props.val.initiatedDate}</div>
                            </div>
                            <div className={`contentPart ${props.selectedObj ? ' hidecontent': ''}`}>
                                <div className='contentHeader'>Product</div>
                                <div>{props.val.product}</div>
                            </div>
                            <div className={`contentPart ${props.selectedObj ? ' hidecontent': ''}`}>
                                <div className='contentHeader'>Component Type</div>
                                <div>{props.val.componentType}</div>
                            </div>
                            <div className={`contentPart ${props.selectedObj ? ' hidecontent': ''}`}>
                                <span className='contentHeader'><i class="fas fa-bars"></i></span>
                            </div>
                            <hr id='rulerproject'/>
                            <div className='statusbar'>
                                <span><span style={{'color':'red'}}>Status</span>: <span style={{'fontWeight':'bold','color':'grey'}}>{props.val.status}</span></span>
                            </div>
                        </div>
    );
}

export default ProjectCard;
